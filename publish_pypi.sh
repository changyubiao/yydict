#!/usr/bin/env bash

# build package
python -m build  --sdist  --wheel


#  不使用隔离环境,跳过依赖检查
 python -m build  -x -n  --sdist  --wheel



# 发布到测试环境
twine upload --repository testpypi dist/*


# 发布到真实的线上环境
twine upload --repository pypi dist/*