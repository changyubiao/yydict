from .yy_dict import YYDict

__version__ = '0.0.2'
__author__ = 'frank,chang'
__license__ = 'MIT'
__copyright__ = 'Copyright 2014-2023 Frank, Chang'
__all__ = ['YYDict']
