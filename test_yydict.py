import unittest
from yydict import YYDict as Dict

TEST_VAL = [1, 2, 3]
TEST_DICT = {'a': {'b': {'c': TEST_VAL}}}


# unittest.TestCase
class AbstractTestsClass(object):
    dict_class = None

    def test_set_one_level_item(self):
        some_dict = {'a': TEST_VAL}
        prop = self.dict_class()
        prop['a'] = TEST_VAL
        self.assertDictEqual(prop, some_dict)

    def test_set_two_level_items(self):
        some_dict = {'a': {'b': TEST_VAL}}
        prop = self.dict_class(some_dict)
        self.assertDictEqual(prop, some_dict)

    #
    def test_set_one_level_property(self):
        prop = self.dict_class({'a': TEST_VAL})
        self.assertEqual(prop['a'], TEST_VAL)
        self.assertEqual(prop.a, TEST_VAL)

        with self.assertRaises(AttributeError):
            print(prop.name)

    def test_emtpy_value(self):
        prop = self.dict_class({})
        self.assertEqual(prop, {})

    def test_emtpy_value02(self):
        prop = self.dict_class(d={})
        self.assertEqual(prop, {})

    def test_emtpy_value03(self):
        prop = self.dict_class(None)
        self.assertEqual(prop, {})

    def test_attribution(self):
        prop = self.dict_class({})
        prop.foo = TEST_VAL
        self.assertEqual(prop.foo, TEST_VAL)

        prop.bar = {'name': 'python'}
        self.assertEqual(prop.bar.name, 'python')

    #
    def test_init_with_dict(self):
        self.assertDictEqual(TEST_DICT, Dict(TEST_DICT))

    #
    def test_init_with_kws(self):
        prop = self.dict_class(a=2, b={'a': 2}, c=[{'a': 2}])
        self.assertDictEqual(prop, {'a': 2, 'b': {'a': 2}, 'c': [{'a': 2}]})

    def test_init_with_tuples(self):
        prop = self.dict_class((("0", 1), ("1", 2), ("2", 3)))
        self.assertDictEqual(prop, {"0": 1, "1": 2, "2": 3})

    #
    @unittest.skip(u"强制跳过 int 类型作为key的示例")
    def test_init_with_list(self):
        """
        测试字典中的 key 是数字的情况
        :return:
        """
        prop = self.dict_class([(0, 1), (1, 2), (2, 3)])
        self.assertDictEqual(prop, {0: 1, 1: 2, 2: 3})

    def test_init_with_list02(self):
        """
        测试 正常的情况
        :return:
        """
        prop = self.dict_class([("0", 1), ("1", 2), ("2", 3)])
        self.assertDictEqual(prop, {"0": 1, "1": 2, "2": 3})

    # @unittest.skip('int key skipped')
    def test_init_with_generator(self):
        prop = self.dict_class(((i, i + 1) for i in range(3)))
        self.assertDictEqual(prop, {0: 1, 1: 2, 2: 3})

    #
    #
    def test_init_raises(self):
        def init():
            self.dict_class(5)

        def init2():
            Dict('a')

        self.assertRaises(TypeError, init)
        self.assertRaises(ValueError, init2)

    def test_init_with_empty_stuff(self):
        a = self.dict_class({})
        b = self.dict_class([])
        self.assertDictEqual(a, {})
        self.assertDictEqual(b, {})

    def test_init_with_list_of_dicts(self):
        prop = self.dict_class({'a': [{'b': 2}]})
        self.assertIsInstance(prop.a[0], self.dict_class)
        self.assertEqual(prop.a[0].b, 2)

    #
    def test_init_with_kwargs(self):
        a = self.dict_class(a='b', c=dict(d='e', f=dict(g='h')))

        self.assertEqual(a.a, 'b')
        self.assertIsInstance(a.c, self.dict_class)

        self.assertEqual(a.c.f.g, 'h')
        self.assertIsInstance(a.c.f, self.dict_class)

    #
    def test_getitem(self):
        prop = self.dict_class(TEST_DICT)
        self.assertEqual(prop['a']['b']['c'], TEST_VAL)

    def test_getattr(self):
        print(f"TEST_DICT:{TEST_DICT}")
        prop = self.dict_class(TEST_DICT)
        self.assertEqual(prop.a.b.c, TEST_VAL)

    def test_isinstance(self):
        self.assertTrue(isinstance(self.dict_class(), dict))

    #
    def test_str(self):
        prop = self.dict_class(TEST_DICT)
        self.assertEqual(str(prop), str(TEST_DICT))

    #
    def test_delitem(self):
        prop = self.dict_class({'a': 2})
        del prop['a']
        self.assertDictEqual(prop, {})

    #
    def test_delitem_nested(self):
        prop = self.dict_class(TEST_DICT)
        del prop['a']['b']['c']
        self.assertDictEqual(prop, {'a': {'b': {}}})

    def test_delattr(self):
        prop = self.dict_class({'a': 2})
        del prop.a
        self.assertDictEqual(prop, {})

    #
    def test_delattr_nested(self):
        prop = self.dict_class(TEST_DICT)
        del prop.a.b.c
        self.assertDictEqual(prop, {'a': {'b': {}}})

    #
    def test_delitem_delattr(self):
        prop = self.dict_class(TEST_DICT)
        del prop.a['b']
        self.assertDictEqual(prop, {'a': {}})

    #
    def test_tuple_key(self):
        prop = self.dict_class()
        prop[(1, 2)] = 2
        self.assertDictEqual(prop, {(1, 2): 2})
        self.assertEqual(prop[(1, 2)], 2)

    def test_set_prop_invalid(self):
        prop = self.dict_class()

        def set_keys():
            prop.keys = 2

        def set_items():
            prop.items = 3

        self.assertRaises(AttributeError, set_keys)
        self.assertRaises(AttributeError, set_items)
        self.assertDictEqual(prop, {})

    def test_dir_with_members(self):
        prop = self.dict_class({'__members__': 1})
        dir(prop)
        self.assertTrue('__members__' in prop.keys())

    #
    def test_to_dict(self):
        nested = {'a': [{'a': 0}, 2], 'b': {}, 'c': 2}
        prop = self.dict_class(nested)
        regular = prop.to_dict()
        self.assertDictEqual(regular, prop)
        self.assertDictEqual(regular, nested)
        self.assertNotIsInstance(regular, self.dict_class)

        def get_attr():
            regular.a = 2

        self.assertRaises(AttributeError, get_attr)

        #
        def get_attr_deep():
            regular['a'][0].a = 1

        #
        self.assertRaises(AttributeError, get_attr_deep)

    def test_to_dict_with_tuple(self):
        nested = {'a': ({'a': 0}, {2: 0})}
        prop = self.dict_class(nested)
        regular = prop.to_dict()
        self.assertDictEqual(regular, prop)
        self.assertDictEqual(regular, nested)
        self.assertIsInstance(regular['a'], tuple)
        self.assertNotIsInstance(regular['a'][0], self.dict_class)

    #
    def test_update_with_lists(self):
        org = self.dict_class()
        org.a = [1, 2, {'a': 'superman'}]
        someother = self.dict_class()
        someother.b = [{'b': 123}]
        org.update(someother)

        correct = {'a': [1, 2, {'a': 'superman'}],
                   'b': [{'b': 123}]}

        org.update(someother)
        self.assertDictEqual(org, correct)
        self.assertIsInstance(org.b[0], dict)

    #
    def test_update_with_kws(self):
        org = self.dict_class(one=1, two=2)
        someother = self.dict_class(one=3)
        someother.update(one=1, two=2)
        self.assertDictEqual(org, someother)

    def test_update_with_keys(self):
        org = self.dict_class(one=1, two=2)
        # add keys
        org.update({'keys': 'test_value'})
        someother = self.dict_class({
            'one': 1, 'two': 2, 'keys': 'test_value'
        })

        self.assertDictEqual(org, someother)

    def test_update_with_keys_2(self):
        """
        覆盖keys 失败
        :return:
        """

        def add_keys():
            org = self.dict_class(one=1, two=2)
            org.keys = 1000

        self.assertRaises(AttributeError, add_keys)

    def test_update_with_args_and_kwargs(self):
        expected = {'a': 1, 'b': 2}
        org = self.dict_class()
        org.update({'a': 3, 'b': 2}, a=1)
        self.assertDictEqual(org, expected)

    #
    def test_update_with_multiple_args(self):
        def update():
            org.update({'a': 2}, {'a': 1})

        org = self.dict_class()
        self.assertRaises(TypeError, update)

    #
    def test_ior_operator_with_lists(self):
        org = self.dict_class()
        org.a = [1, 2, {'a': 'superman'}]
        someother = self.dict_class()
        someother.b = [{'b': 123}]
        org |= someother

        correct = {'a': [1, 2, {'a': 'superman'}],
                   'b': [{'b': 123}]}

        org |= someother
        self.assertDictEqual(org, correct)
        self.assertIsInstance(org.b[0], dict)

    def test_ior_operator_with_dict(self):
        org = self.dict_class(one=1, two=2)
        someother = self.dict_class(one=3)
        someother |= dict(one=1, two=2)
        self.assertDictEqual(org, someother)

    #

    #
    def test_or_operator_with_lists(self):
        org = self.dict_class()
        org.a = [1, 2, {'a': 'superman'}]
        someother = self.dict_class()
        someother.b = [{'b': 123}]

        # 或 运算符
        org = org | someother

        correct = {'a': [1, 2, {'a': 'superman'}],
                   'b': [{'b': 123}]}

        org = org | someother
        self.assertDictEqual(org, correct)
        self.assertIsInstance(org.b[0], dict)

    #
    def test_ror_operator(self):
        org = dict()
        org['a'] = [1, 2, {'a': 'superman'}]
        someother = self.dict_class()
        someother.b = [{'b': 123}]
        org = org | someother
        self.assertIsInstance(org, dict)

        correct = {'a': [1, 2, {'a': 'superman'}],
                   'b': [{'b': 123}]}

        org = someother | org
        self.assertDictEqual(org, correct)
        self.assertIsInstance(org, Dict)
        self.assertIsInstance(org.b[0], Dict)

    #
    def test_or_operator_type_error(self):
        old = self.dict_class()
        with self.assertRaises(TypeError):
            old | 'test'

    def test_ror_operator_type_error(self):
        old = self.dict_class()
        with self.assertRaises(TypeError):
            'test' | old

    def test_init_from_zip(self):
        keys = ['a']
        values = [42]
        items = zip(keys, values)
        d = self.dict_class(items)
        self.assertEqual(d.a, 42)

    #
    def test_setdefault_simple(self):
        d = self.dict_class()
        d.setdefault('a', 2)
        self.assertEqual(d.a, 2)
        d.setdefault('a', 3)
        self.assertEqual(d.a, 2)
        d.setdefault('c', []).append(2)
        self.assertEqual(d.c, [2])


class DictTests(unittest.TestCase, AbstractTestsClass):
    dict_class = Dict


"""
Allow for these test cases to be run from the command line
via `python test_yydict.py`
"""
if __name__ == '__main__':
    test_classes = (DictTests,)
    loader = unittest.TestLoader()
    runner = unittest.TextTestRunner(verbosity=2)
    for class_ in test_classes:
        loaded_tests = loader.loadTestsFromTestCase(class_)
        runner.run(loaded_tests)
